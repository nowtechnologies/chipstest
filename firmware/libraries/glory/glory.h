#ifndef __GLORY_H__
#define __GLORY_H__

#include "Arduino.h"
/// MOUSE MODES
#define ABSOLUTE 1
#define RELATIVE 2

/// CONFIG DEFAULTS
#define DEFAULT_MOUSE_MODE RELATIVE
#define DEFAULT_RANGE 	   50
#define DEFAULT_FILTER 	   50
#define DEFAULT_OFFSET 	   0
#define DEFAULT_GROUP 	   2
#define	DEFAULT_ID 		   	 4
#define	DEFAULT_SERIAL 	   1
#define	DEFAULT_POWER 	   0
#define DEFAULT_ACATKEY    0xCD
#define DEFAULT_ACATSTATE  0
#define DEFAULT_SCROLL     50
#define DEFAULT_AUTORANGE  0
#define DEFAULT_MOUSESPEED 25

/// UART COMMAND IDENTIFIERS
	#define SETSTREAM     1 // DEPRECATED
	#define SETRANGE      2
	#define SETFILTER     3
	#define SETOFFSET     4
	#define SETPAN        5
	#define GETVERSION    6
	#define SETIDFILTER   7
	#define GETRANGE      8
	#define SETZERO       9
	#define SWITCHCLICK   10
	#define SCROLL        11
	#define GETFILTER     12
	#define GETOFFSET     13
	#define DEFAULTS	  	14
	#define GETSERIAL     15
	#define GETREVISION   16
	#define KEYPRESS	  	17
	#define AUTOZERO	  	18
	#define SETTYPE		  	19
	#define GETTYPE       20
	#define SETACAT		  	21
	#define PASSTHROUGH   22
	#define SCROLLSPEED   23
	#define GETSPEED      24
	#define SETMOUSEMODE  25
	#define SETMOUSESPEED 26
	#define GETMOUSESPEED 27
	#define SETAUTORANGE  28
	#define CLEARROM	  	29
	#define GETMOUSEMODE  30
	#define CHKGLORYCABLE 31
	#define HOPTOCHANNEL  32


	/// MODES
	#define IDLE 					0
	#define INACTIVE 			1
	#define ACTIVE 				2

	/// KEYBOARD
	#define SINGLEKEY 		0
	#define PRESS	  			1
	#define RELEASE   		2
	#define CLEAR     		3

class Glory{
public:

struct Eulers{
	float Psi;
	float Theta;
	float Phi;
};
Eulers eulers;

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max);
struct Eulers getEuler(float w, float x, float y, float z);
float getMotion();
struct Eulers substract(Eulers a, Eulers b);
struct Eulers getDifference(Eulers a, Eulers b);
void filter_head();
void calculate_mouse(bool active);
void filter_blink_slow(int infrared);
void filter_blink_fast(int infrared);
void calculate_blink_offset(int infrared);
void calculate_blink(int sn, bool awake);
void setRange(byte angular);
void resetRange();
void setFilter(byte efficient);
void changeClick(byte thisButton);
void setOffset(byte offset);
void setAcat(bool state);
void setScroll(bool m, bool s);
void reportChange(int sn, byte mode, byte change);
void setAcatKey(byte m);
void setScrollSpeed(byte percent);
void reloadRange();
void auto_range(bool active);
void setMouseMode(byte m);
void setAutoRange(bool on);
void setMouseSpeed(byte p);

};
#endif
