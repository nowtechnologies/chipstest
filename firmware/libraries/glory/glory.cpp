	// TODO:
	// scroll: one slider for abs and rel
	// filters
	// reset autorange to minimum

	#include "glory.h"
	#include "config.h"
	#include "terminal.h"

	bool  ACAT = false;
	byte  AcatKey = 0xCD;
	bool  rightClick = false;

	float ang_x = 0.50f;
	float ang_y = ang_x/2;
	float min_range = 0.00f;
	float max_range = 1.00f;

	float mouse_filter = 0.025f; //filter
	float min_filter = 0.025;
	float max_filter = 0.0001;

	float blink_offset = 400.0f; //offset

	float roll = 0.0f;
	float pitch = 0.0f;
	float yaw = 0.0f;
	float roll_zero = 0.0f;
	float yaw_zero = 0.0f;
	float mouse_x = 0.0f;
	float mouse_y = 0.0f;
	float mx = 0.0f;
	float my = 0.0f;
	float rmx = 0.0f;
	float rmy = 0.0f;
	int32_t minVal = -1L;
	int32_t maxVal = 32767L;

	bool  scrollMode = false;
	bool  scroll_lock = false;
	bool  singleScroll = true;
	float scroll_counter = 0;
	float scroll = 0;
	float scroll_start_y = 0.0f;
	float scroll_start_x = 0.0f;
	int   scroll_limit = maxVal/5;
	float scroll_speed = 0.025f;
	float scroll_min = 0.01f;
	float scroll_max = 0.03f;
	int   rel_scroll_speed = 5;
	float diff_mx, diff_my;
	float vertical_speed;

	float slow_filter = 0.0001f;
	float slow_filter_output = 0.0f;
	float fast_filter = 0.5f;
	float fast_filter_output = 0.0f;
	int   filter_counter = 0;
	int   mouse_click_time = 10;

	int windowSize = 10;

	int   averageCounter = 0;
	float movingAverage = 0.0f;
	float accumulator = 0.0f;
	float integral = 0.0f;

	float offset_divider = 1.0f;
	float top,bottom,left,right;

	float rel_speed = 100.0f;
	bool auto_r_on = false;
	byte mouse_mode = 0;

	byte mouse_report[] = { 0x55, 0xAA, DONGLE_TYPE, SWITCHCLICK, 9, 0, 0, 0, 0 };

	void Glory::reportChange(int sn, byte inMode, byte change){
		mouse_report[3] = inMode;
		mouse_report[5] = lowByte(sn);
		mouse_report[6] = highByte(sn);
		mouse_report[7] = change;

		for (uint16_t i = 0; i <= sizeof(mouse_report)-1; i++){
			Serial.write(mouse_report[i]);
		}
	}

	void Glory::setMouseSpeed(byte percent){
		percent = constrain(percent, 1, 100);
		rel_speed = mapfloat(percent, 1, 100, 10, 50);
	  vertical_speed = (rel_speed * 16)/9;
	}

	void Glory::setScrollSpeed(byte percent){
		percent = constrain(percent, 0, 100);
		scroll_speed = mapfloat(percent, 0, 100, scroll_min, scroll_max);
		rel_scroll_speed = map(percent, 0, 100, 10, 1);
	}

	void Glory::setMouseMode(byte m){
		mouse_mode = constrain(mouse_mode, 1, 2);
		mouse_mode = m;
	}

	void Glory::resetRange(){
		left     =  ang_x / 2;
		right    = -ang_x / 2;
		top      = -ang_y / 2;
		bottom   =  ang_y / 2;
	}

	void Glory::setRange(byte angular){
		angular  = constrain(angular, 1, 100);
		ang_x    = mapfloat(angular, 0, 100, min_range, max_range);
		ang_y    = (ang_x * 9) / 16; // this shall be the screen ratio
		resetRange();
	}

	void Glory::setFilter(byte efficient){
		efficient	 = constrain(efficient, 0, 100);
		mouse_filter = mapfloat(efficient, 0, 100, min_filter, max_filter);
	}

	void Glory::setOffset(byte offset){
		offset = constrain(offset, 0, 100);
		offset_divider = mapfloat(offset, 0, 100, 1.0f, 10.0f);
	}

	void Glory::setAcat(bool state){
		ACAT = state;
		Serial.println(state);
	}

	void Glory::setAcatKey(byte b){
		AcatKey = b;
	}

	void Glory::setAutoRange(bool on){
		auto_r_on = on;
	}

	void Glory::setScroll(bool m, bool s){
		scrollMode = m;
		singleScroll = s;
		scroll_lock = false;
	}

	//MATH///////////////////////////////////////////////////////////////////////////////

	float Glory::mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
	{
	  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}

	Glory::Eulers Glory::getEuler(float w, float x, float y, float z) {

		// Rotation around the z,y and x directions, respectively.
		Eulers eulers;
		eulers.Psi 	 = 0.0f;
		eulers.Theta = 0.0f;
		eulers.Phi 	 = 0.0f;

	  // Store quaternion values in local vars
	  float q0 = w;
	  float q1 = x;
	  float q2 = y;
	  float q3 = z;

  	float R11,R12,R13,R23,R33; // Elements of the rotation matrix corresponding to the quaternion.
	  float qnorm=sqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
	  q0=q0/qnorm;
	  q1=q1/qnorm;
	  q2=q2/qnorm;
	  q3=q3/qnorm;

	  // Calculate elements of the rotation matrix.
	  R11=2.0f*(q0*q0 + q1*q1) - 1.0f;
	  R12=2.0f*(q1*q2 + q0*q3);
	  R13=2.0f*(q1*q3 - q0*q2);
	  R23=2.0f*(q2*q3 + q0*q1);
	  R33=2.0f*(q0*q0 + q3*q3) - 1.0f;

	  // If some of these elements are zero, then we shift them
	  // in order to avoid NaN values of Psi, Phi and Theta.
	  if(R11==0) { R11+=0.000001; }
	  if(R33==0) { R33+=0.000001; }

	  // Calculate the Euler angles in radians

	  eulers.Psi 	 = atan2(R12,R11) + PI;
	  eulers.Theta = asin(R13) + PI;
	  eulers.Phi 	 = atan2(R23,R33) + PI;

		return eulers;
	}

	Glory::Eulers Glory::substract(Eulers a, Eulers b){
		Eulers sub;
		sub.Theta = a.Theta-b.Theta;
		sub.Phi 	= a.Phi - b.Phi;
		sub.Psi 	= a.Psi - b.Psi;
		return sub;
	}

	Glory::Eulers Glory::getDifference(Eulers head, Eulers chair){
  Eulers diff;

		if (head.Psi > chair.Psi + PI) { diff.Psi = head.Psi - chair.Psi - TWO_PI; }
	  else if (head.Psi < chair.Psi - PI) { diff.Psi = head.Psi - chair.Psi + TWO_PI; }
	  else { diff.Psi = head.Psi - chair.Psi; }

		if (head.Phi > chair.Phi + PI) { diff.Phi = head.Phi - chair.Phi - TWO_PI; }
	  else if (head.Phi < chair.Phi - PI) { diff.Phi = head.Phi - chair.Phi + TWO_PI; }
	  else { diff.Phi = head.Phi - chair.Phi; }

		if (head.Theta > chair.Theta + PI) { diff.Theta = head.Theta - chair.Theta - TWO_PI; }
	  else if (head.Theta < chair.Theta - PI) { diff.Theta = head.Theta - chair.Theta + TWO_PI; }
	  else { diff.Theta = head.Theta - chair.Theta; }

		return diff;
	}

	//////////////////////////////////////////////////////////////////

	void Glory::filter_blink_slow(int infrared){
	  float difference = infrared - slow_filter_output;
	  if (abs(difference) > 1) {
	    slow_filter_output += difference * slow_filter;
	  }
	}

	void Glory::filter_blink_fast(int infrared){
	  float difference = infrared - fast_filter_output - blink_offset;
	  if (abs(difference) > 1) {
	    fast_filter_output += difference * fast_filter;
	  }
	}

	void Glory::calculate_blink_offset(int infrared){
		accumulator = accumulator + infrared;
		integral = integral + sq(infrared - movingAverage);

		averageCounter++; if (averageCounter == windowSize - 1) {
			averageCounter = 0;
			movingAverage  = accumulator / windowSize;
			float intAvrg  = integral / windowSize;
			blink_offset = sqrt(intAvrg) / offset_divider;
			accumulator = 0.0f;
			integral = accumulator;
		}
	}

	byte buttonCode = 0;
	void changeClick(byte thisButton){
		buttonCode = thisButton;
	};

	boolean mouse_released = true;

	void Glory::calculate_blink(int sn, bool awake){

	  if (fast_filter_output > slow_filter_output) filter_counter++;
	  else filter_counter-=20;
	  filter_counter = constrain(filter_counter, 0, 100000);


	  if (filter_counter >= mouse_click_time) { // sensor is above threshold
	    if (mouse_released) { // if the previous state was released then press something

			if (!ACAT){ // if not pressing a single key then press a mouse button
				if (awake){
				    if (scrollMode) { // unless we are in scroll mode
						scroll_lock = true;
					}
					else {
						/*
							switch(buttonCode){
								case 0:
									Mouse.press(MOUSE_LEFT);
									Mouse.click();
								break;
								case 1:
									Mouse.press(MOUSE_RIGHT);
									Mouse.click();
								break;
								case 2:
									Mouse.press(MOUSE_MIDDLE);
									Mouse.click();
								break;
							}
							*/
					}
				}
			}
			else {
				//Keyboard.press(AcatKey); // press the defined key
			}
		    mouse_released = false; // we are done, make the state pressed
	    }
	  }

	  else { // sensor is below threshold

			scroll_start_y = mouse_y;
			scroll_start_x = mouse_x;

		    if (!mouse_released) { // if the previous state was pressed then release something

				if (!ACAT) {	// if not releasing a single key then release a mouse button

					if (scrollMode) { // unless we are in scroll mode
						scroll_lock = false;
		  			    scroll_counter = 0;
						if (singleScroll) {
							scrollMode = false; // auto quit scroll mode
							//reportChange(sn, SCROLL, 0);
						}
					}
					else { // was not scrolling
							//Mouse.release(MOUSE_LEFT);
							//Mouse.release(MOUSE_RIGHT);
							//Mouse.release(MOUSE_MIDDLE);
							//Mouse.click();
							buttonCode = 0;
							//reportChange(sn, SWITCHCLICK, 0); // if not the default key is released report to UI
						}

				} // eof: !ACAT

				else { // acat mode
				//	Keyboard.release(AcatKey); // relese the defined key
				}

			  mouse_released = true; // we are done, make the state released
		    }

		} // eof: below threshold

	}; // eof: calculate_blink
