
	#include "Arduino.h"
	#include "onebutton.h"
	#include "config.h"

	long press_counter = 0;
    	byte pressed = 0;
	bool set = false;

	void oneButton::initialize(){
		pinMode(BUTTON_PIN, INPUT);
     		digitalWrite(BUTTON_PIN, HIGH);
	};


	byte oneButton::press(){

	   // if the button was previously pressed either long or short clear states
	   if ( (pressed == 1) || (pressed == 2) ) {
		   pressed = 0;
		   //press_counter = 0;
	   }

	   // the button is pressed
	   if ( digitalRead(BUTTON_PIN) == 0 ) {
		   press_counter++;
		   delay(5);
		   press_counter = constrain(press_counter, 0, SHORTPRESS*2);

		   if (press_counter >= SHORTPRESS*2 && !set) {
			   pressed = 2;
			   set = true;
			   return pressed;
		   }
	   }
	   // the button is released
	   else
		{  // WARNING: this process runs after button release
		   if ( (press_counter > 5) && (press_counter <= SHORTPRESS) ) {
			pressed = 1;
		   }
	   	   press_counter = 0;
		   set = false;
	    }

	return pressed;
	}
