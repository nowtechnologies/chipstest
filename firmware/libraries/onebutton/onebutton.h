#ifndef __ONEBUTTON_H__
#define __ONEBUTTON_H__

#include "config.h"

	#define ACTIVE 2
	#define INACTIVE 1
	#define MOUSE 1
	#define CONTROLLER 2
	#define ANDROID 3

	class oneButton {
		public:
			void initialize();
			byte press();
	};
	
#endif