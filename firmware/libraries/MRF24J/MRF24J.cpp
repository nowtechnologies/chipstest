#include <MRF24J.h>
#include <config.h>

uint8_t SLP;
SPISettings MRF::spiSettings = SPISettings(8000000, MSBFIRST, SPI_MODE0); // SPI_MODE0

MRF::MRF(Mrf24j40Type type, int cs) {
  this->cs = cs;
  this->type = type;
  this->panId = 0;
  this->srcAddr = 1;
  this->seqNumber = 0;
  this->txCount = 0;
  this->rxSize = 0;
  this->rxCount = 0;
  this->rssi = 0;
  this->lqi = 0;
}

void MRF::begin() {
  
  pinMode(MRF_WAKEPIN, OUTPUT);
  digitalWrite(MRF_WAKEPIN, HIGH);
  pinMode(MRF_RESET, OUTPUT);
  digitalWrite(MRF_RESET, HIGH);
  pinMode(cs, OUTPUT);
  digitalWrite(cs, HIGH);
  SPI.begin();
  Serial.println("SPI : OK");
  init();
}

void MRF::setPanId(uint16_t panId) {
  this->panId = panId;
  writeShort(MRF_PANIDH, panId >> 8);
  writeShort(MRF_PANIDL, panId);
}

void MRF::setAddress(uint16_t addr) {
  this->srcAddr = addr;
  writeShort(MRF_SADRH, addr >> 8);
  writeShort(MRF_SADRL, addr);
}

void MRF::setCoordinator(bool coord) {
  uint8_t reg = readShort(MRF_RXMCR);
  writeShort(MRF_RXMCR, coord ? reg | 0b00001100 : reg & ~0b00001100);
}

void MRF::setChannel(int channel) {
  if (channel < 11 || channel > 26) {
    return;
  }

  // Change channel
  writeLong(MRF_RFCON0, (channel - 11) << 4 | 0x03);
  
  // Perform RF state machine reset and wait for RF circuitry to calibrate
  writeShort(MRF_RFCTL, 0b00000100);
  writeShort(MRF_RFCTL, 0);
  delayMicroseconds(200);
}

/*
	RFCON3

	[ 7 | 6 ] bit 
	  1 | 1 = -30db
	  1 | 0 = -20db
	  0 | 1 = -10db
	  0 | 0 =  0db
      
	[ 5 | 4 | 3 ] bit
	  1 | 1 | 1 = -6.3db
	  1 | 1 | 0 = -4.9db
	  1 | 0 | 1 = -3.7db
	  1 | 0 | 0 = -2.8db
	  0 | 1 | 1 = -1.9db
	  0 | 1 | 0 = -1.2db
	  0 | 0 | 1 = -0.5db
	  0 | 0 | 0 =  0db

	[ 2 | 1 | 0 ] = maintain as zero
	
*/

void MRF::setPower(uint8_t percent){
	percent = constrain(percent, 0, 100);
	uint8_t power = map(percent, 0, 100, 31, 0);	
	writeLong(MRF_RFCON3, power << 3);
}

float MRF::measureSignalStrength() {
  // Disable PA
  if (type != MRF24J40MA) {
    writeLong(MRF_TESTMODE, 0b00001000);  // Configure RF state machine for normal operation
    writeShort(MRF_TRISGPIO, 0b00000110); // Configure GPIO1 and GPIO2 for output
    writeShort(MRF_GPIO, 0b00000100);     // Set GPIO1 0 to disable PA and GPIO2 to 1 to enable LNA
  }

  // Initiate RSSI calculation (RSSIMODE1 = 1)
  writeShort(MRF_BBREG6, 0b11000000);
  
  // Wait until conversion is ready and read RSSI
  while (!(readShort(MRF_BBREG6) & 0b00000001));
  rssi = readLong(MRF_RSSI);

  // Re-enable PA
  enablePaLna();

  return getSignalStrength();
}

float MRF::getSignalStrength() {
  return rssi * 0.197948 - 87.646977;
}

int MRF::getLinkQuality() {
  return lqi;
}

void MRF::sleep() {

  switch (type) {

	case MRF24J40MA:
		
	/*  RXFLUSH 0x0D
		------------
		bit 7 - Reserved: Maintain as ‘0’ 
		
		bit 6 - WAKEPOL: Wake Signal Polarity bit
			1 = Wake signal polarity is active-high
			0 = Wake signal polarity is active-low (default)
		
		bit 5 - WAKEPAD: Wake I/O Pin Enable bit 1 = Enable wake I/O pin
			0 = Disable wake I/O pin (default) 
		
		bit 4 - Reserved: Maintain as ‘0’
		
		bit 3 - CMDONLY: Command Frame Receive bit
			1 = Only command frames are received, all other frames are filtered out
			0 = All valid frames are received (default) 
		
		bit 2 - DATAONLY: Data Frame Receive bit
			1 = Only data frames are received, all other frames are filtered out 
			0 = All valid frames are received (default)
		
		bit 1 - BCNONLY: Beacon Frame Receive bit
			1 = Only beacon frames are received, all other frames are filtered out 
			0 = All valid frames are received (default)
		
		bit 0 - RXFLUSH: Reset Receive FIFO Address Pointer bit
			1 = Resets the RXFIFO Address Pointer to zero. RXFIFO data is not modified. Bit is automatically cleared to ‘0’ by hardware.
	*/
		
    	writeShort(MRF_SOFTRST, 0b00000100); // Perform Power Management Reset (RSTPWR = 1)
	    writeShort(MRF_WAKECON, 0b10000000); // Enable Immediate Wake-up Mode (IMMWAKE = 1)
		//digitalWrite(MRF_WAKEPIN, LOW);			// WAKE PIN false
	    writeShort(MRF_SLPACK, 0b10000000);  // Put the module to sleep (SLPACK = 1)
		/*
		writeShort(MRF_WAKECON, 0b10000000); // Enable Immediate Wake-up Mode (IMMWAKE = 1)
		SLP = readShort(MRF_SLPACK);
		digitalWrite(MRF_WAKEPIN, LOW);			// WAKE PIN false

		writeShort(MRF_SOFTRST, 0b00000100);    // Perform Power Management Reset (RSTPWR = 1)
		//writeShort(MRF_BBREG1, 1); 				// Disable RX
		writeShort(MRF_SLPACK, SLP | _SLPACK);  // Put the module to sleep 
	    //writeShort(MRF_SLPACK, 0b10000000);  // Put the module to sleep (SLPACK = 1)
		
		*/
		
	break;
    case MRF24J40MB:
		// GPIO1 and GPIO2 are connected
		writeLong(MRF_TESTMODE, 0b00001000);  // Configure RF state machine for normal operation
		writeShort(MRF_TRISGPIO, 0b00000110); // Configure GPIO1 and GPIO2 for output
		writeShort(MRF_GPIO, 0);              // Set GPIO1 and GPIO2 to 0 to disable PA and LNA
	break;
    case MRF24J40MC:
		// GPIO1, GPIO2 and GPIO3 are connected - GPIO3 enables (high) or disables (low) the PA voltage regulator
		writeLong(MRF_TESTMODE, 0b00001000);  // Configure RF state machine for normal operation
		writeShort(MRF_TRISGPIO, 0b00001110); // Configure GPIO1, GPIO2 and GPIO3 for output
		writeShort(MRF_GPIO, 0);              // Set GPIO1, GPIO2 and GPIO3 to 0 to disable PA, LNA and PA regulator
	break;
    case MRF24J40MD:
    case MRF24J40ME:
		// GPIO0, GPIO1 and GPIO2 are connected
		writeLong(MRF_TESTMODE, 0b00001000);  // Configure RF state machine for normal operation
		writeShort(MRF_TRISGPIO, 0b00000111); // Configure GPIO0, GPIO1 and GPIO2 for output
		writeShort(MRF_GPIO, 0);              // Set GPIO1 and GPIO2 to 0 to disable PA and LNA
	break;
  }

}

void MRF::wakeup() {

	//digitalWrite(MRF_WAKEPIN, HIGH);
	reset(); // Perform software reset
	init();  // Reinitialize all registers	

	/*	
	digitalWrite(MRF_WAKEPIN, HIGH);
	clear();
	//writeShort(MRF_BBREG1, 0); // Enable RX
	*/
}

bool MRF::write(uint8_t b) {
  return writeToBuffer(&b, 1);
}

uint8_t MRF::read() {
  uint8_t result;
  return readFromBuffer(&result, 1) ? result : 0;
}

bool MRF::writeInt(int16_t i) {
  return writeToBuffer(&i, 2);
}

int16_t MRF::readInt() {
  int16_t result;
  return readFromBuffer(&result, 2) ? result : 0;
}

bool MRF::writeUnsignedInt(uint16_t u) {
  return writeToBuffer(&u, 2);
}

uint16_t MRF::readUnsignedInt() {
  uint16_t result;
  return readFromBuffer(&result, 2) ? result : 0;
}

bool MRF::writeLong(int32_t l) {
  return writeToBuffer(&l, 4);
}

int32_t MRF::readLong() {
  int32_t result;
  return readFromBuffer(&result, 4) ? result : 0;
}

bool MRF::writeUnsignedLong(uint32_t u) {
  return writeToBuffer(&u, 4);
}

uint32_t MRF::readUnsignedLong() {
  uint32_t result;
  return readFromBuffer(&result, 4) ? result : 0;
}

bool MRF::writeFloat(float f) {
  return writeToBuffer(&f, sizeof(float));
}

float MRF::readFloat() {
  float result;
  return readFromBuffer(&result, sizeof(float)) ? result : 0;
}

int MRF::writeBytes(uint8_t* buf, int size) {
  if (size > bytesLeftToWrite()) {
    size = bytesLeftToWrite();
  }

  writeToBuffer(buf, size);
  return size;
}

int MRF::readBytes(uint8_t* buf, int size) {
  if (size > bytesLeftToRead()) {
    size = bytesLeftToRead();
  }

  readFromBuffer(buf, size);
  return size;
}

int MRF::writeString(char* str) {
  if (bytesLeftToWrite() == 0) {
    return 0;
  }

  int size = strlen(str);
  
  if (size > bytesLeftToWrite() - 1) {
    size = bytesLeftToWrite() - 1;
  }

  writeToBuffer(str, size);
  write('\0');
  return size + 1;
}

const char* MRF::readString(char* str, int size) {
  char* result = (char*)&rxBuf[rxCount];

  if (str == NULL) {
    return result;
  }

  if (size > bytesLeftToRead()) {
    size = bytesLeftToRead();
  }
  
  if (size == 0) {
    return 0;
  }

  str[size - 1] = '\0';
  strncpy(str, (char*)&rxBuf[rxCount], size - 1);
  rxCount += strlen(str) + 1;
  return result;
}

void MRF::startPacket() {
  txCount = 0;
}

int MRF::bytesLeftToWrite() {
  return MRF_MAX_PAYLOAD_SIZE - txCount;
}

bool MRF::sendPacket(uint16_t addr, bool ack) {
  if (txCount == 0) {
    return false;
  }

  // Send header size, frame size and header
  int i = 0;
  writeLong(i++, MRF_MHR_SIZE);                  // Header size
  writeLong(i++, MRF_MHR_SIZE + txCount);        // Frame size
  writeLong(i++, ack ? 0b01100001 : 0b01000001); // Frame control bits 0-7 (data frame, no security, no pending data, ack disabled, intra-PAN)
  writeLong(i++, 0b10001000);                    // Frame control bits 8-15 (16-bit addresses with PAN)
  writeLong(i++, seqNumber++);                   // Sequence number
  writeLong(i++, panId);                         // PAN ID
  writeLong(i++, panId >> 8);
  writeLong(i++, addr);                          // Destination address
  writeLong(i++, addr >> 8);
  writeLong(i++, srcAddr);                       // Source address
  writeLong(i++, srcAddr >> 8);

  // Send data payload
  for (int j = 0; j < txCount; j++) {
    writeLong(i++, txBuf[j]);
  }

  // Start transmission (0x37 0x01)
  writeShort(MRF_TXNCON, ack ? 0b00000101 : 0b00000001);
  
  return true;
}

bool MRF::transmissionDone() {
  return readShort(MRF_INTSTAT) & 0b00000001;
}

bool MRF::transmissionSuccess() {
  return !(readShort(MRF_TXSTAT) & 0b00000001);
}

void MRF::RXcopy(){

    int frameSize;
    // Packet received, get the number of bytes
    frameSize = readLong(0x300);
    
    // Copy the message bytes into the user buffer
    rxSize = 0;
    rxCount = 0;
    while (rxSize < (frameSize - MRF_MHR_SIZE - MRF_MFR_SIZE)) {
      rxBuf[rxSize] = readLong(0x301 + MRF_MHR_SIZE + rxSize);
      rxSize++;
    }
    
	/*
    // Read RSSI and LQI
    lqi = readLong(0x301 + frameSize);
    rssi = readLong(0x301 + frameSize + 1);
  	*/
}

void MRF::clearISR(){
	readShort(MRF_INTSTAT); // clear isr
}

void MRF::enableRX(bool yes){
	if (!yes) writeShort(MRF_BBREG1, 0b00000100);	// disable receiver
	else	  writeShort(MRF_BBREG1, 0b00000000);	// enable receiver
}

void MRF::flushRX(){
	writeShort(MRF_RXFLUSH, 0b00000001); // flush rx hw fifo 
}

bool MRF::receivePacketOnNoInt() {
  int frameSize;

  // Check RXIF in INTSTAT
  if (readShort(MRF_INTSTAT) & 0b00001000) {
    // Disable receiver
    writeShort(MRF_BBREG1, 0b00000100);
    
    // Packet received, get the number of bytes
    frameSize = readLong(0x300);
    
    // Copy the message bytes into the user buffer
    rxSize = 0;
    rxCount = 0;
    while (rxSize < (frameSize - MRF_MHR_SIZE - MRF_MFR_SIZE)) {
      rxBuf[rxSize] = readLong(0x301 + MRF_MHR_SIZE + rxSize);
      rxSize++;
    }
    
    // Read RSSI and LQI
    lqi = readLong(0x301 + frameSize);
    rssi = readLong(0x301 + frameSize + 1);
  
    // Flush the reception buffer, re-enable receiver
    writeShort(MRF_RXFLUSH, 0b00000001);
    writeShort(MRF_BBREG1, 0);
    
    // Wait until RXIF is cleared (takes a while)
    while(readShort(MRF_INTSTAT) & 0b00001000);
    
    return true;
  }
  
  return false;
}

int MRF::bytesLeftToRead() {
  return rxSize - rxCount;
}

void MRF::reset() {
  writeShort(MRF_SOFTRST, 0b00000111); // Perform full software reset (RSTPWR = 1, RSTBB = 1, RSTMAC = 1)
}

void MRF::clear(){
	uint8_t old = readShort(MRF_RFCTL);
	writeShort(MRF_RFCTL, old | RFRST);
	writeShort(MRF_RFCTL, old & ~RFRST);
	delay(2);
}

void MRF::init() {
	
  reset();	
  // MRF24J40 module configuration
  writeShort(MRF_PACON2, 0b10011000);    // Setup recommended PA/LNA control timing (TXONTS = 0x6)
  writeShort(MRF_TXSTBL, 0b10010101);    // Setup recommended PA/LNA control timing (RFSTBL = 0x9)
  writeLong(MRF_RFCON0, 0b00000011);     // Set recommended value for RF Optimize Control (RFOPT = 0x3)
  writeLong(MRF_RFCON1, 0b00000010);     // Set recommended value for VCO Optimize Control (VCOOPT = 0x2)
  writeLong(MRF_RFCON2, 0b10000000);     // Enable PLL (PLLEN = 1)
  writeLong(MRF_RFCON6, 0b10010000);     // Set recommended value for TX Filter Control and 20MHz Clock Recovery Control
                                         //  (TXFIL = 1, 20MRECVR = 1)
  writeLong(MRF_RFCON7, 0b10000000);     // Use 100kHz internal oscillator for Sleep Clock (SLPCLKSEL = 0x2)
  writeLong(MRF_RFCON8, 0b00010000);     // Set recommended value for VCO Control (RFVCO = 1)
  writeLong(MRF_SLPCON0, 0b00000001);    // Disable the sleep clock to save power (/SLPCLKEN = 1)
  writeLong(MRF_SLPCON1, 0b00100001);    // Disable CLKOUT pin and set Sleep Clock Divisor to minimum of 0x01 for the
                                         //  100kHz internal oscillator (/CLOUKTEN = 1, SLPCLKDIV = 0x01)
  writeShort(MRF_BBREG2, 0b10111000);    // Use CCA Mode 1 - Energy above threshold - and set CCA Carrier Sense
                                         //  Threshold to recommended value (CCAMODE = 0x2, CCACSTH = 0xE)
  writeShort(MRF_CCAEDTH, 0b01100000);   // Set energy detection threshold to recommended value (CCAEDTH = 0x60)
  writeShort(MRF_BBREG6, 0b01000000);    // Calculate RSSI for each packet received (RSSIMODE2 = 1)
  writeShort(MRF_INTCON, 0b11110111);    // Enable RX FIFO reception interrupt (RXIE = 0)

  setPanId(panId);  
  //setChannel(11);                        // Set default channel (must keep 11) and reset state machine
  //setPower(50);
  
  delay(2);                              // Let RF stabilize
}

void MRF::enablePaLna() {
  if (type != MRF24J40MA) {
    writeShort(MRF_TRISGPIO, 0);          // Configure all GPIO pins as input
    writeShort(MRF_GPIO, 0);
    writeLong(MRF_TESTMODE, 0b00001111);  // Configure RF state machine for automatic PA/LNA control
  }
}

uint8_t MRF::readShort(uint8_t addr) {
  SPI.beginTransaction(spiSettings);
  digitalWrite(cs, LOW);
  SPI.transfer(addr << 1 & 0b01111110);
  uint8_t result = SPI.transfer(0);
  digitalWrite(cs, HIGH);
  SPI.endTransaction();
  return result;
}

void MRF::writeShort(uint8_t addr, uint8_t data) {
  SPI.beginTransaction(spiSettings);
  digitalWrite(cs, LOW);
  SPI.transfer((addr << 1 & 0b01111110) | 0b00000001);
  SPI.transfer(data);
  digitalWrite(cs, HIGH);
  SPI.endTransaction();
}

uint8_t MRF::readLong(uint16_t addr) {
  SPI.beginTransaction(spiSettings);
  digitalWrite(cs, LOW);
  SPI.transfer(addr >> 3 | 0b10000000);
  SPI.transfer(addr << 5);
  uint8_t result = SPI.transfer(0);
  digitalWrite(cs, HIGH);
  SPI.endTransaction();
  return result;
}

void MRF::writeLong(uint16_t addr, uint8_t data) {
  SPI.beginTransaction(spiSettings);
  digitalWrite(cs, LOW);
  SPI.transfer(addr >> 3 | 0b10000000);
  SPI.transfer(addr << 5 | 0b00010000);
  SPI.transfer(data);
  digitalWrite(cs, HIGH);
  SPI.endTransaction();
}

bool MRF::writeToBuffer(void* data, int size) {
  if (size > bytesLeftToWrite()) {
    return false;
  }

  memcpy(&txBuf[txCount], data, size);
  txCount += size;
  return true;
}

bool MRF::readFromBuffer(void* data, int size) {
  if (size > bytesLeftToRead()) {
    return false;
  }

  memcpy(data, &rxBuf[rxCount], size);
  rxCount += size;
  return true;
}
