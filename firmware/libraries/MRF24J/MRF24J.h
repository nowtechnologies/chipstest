#ifndef MRF24J_h
#define MRF24J_h

#include <Arduino.h>
#include <SPI.h>

/* INTSTAT */
#define SLPIF		(1<<7)
#define WAKEIF		(1<<6)
#define HSYMTMRIF	(1<<5)
#define SECIF		(1<<4)
#define RXIF		(1<<3)
#define TXG2IF		(1<<2)
#define TXG1IF		(1<<1)
#define TXNIF		(1)

/* INTCON */
#define SLPIE		(1<<7)
#define WAKEIE		(1<<6)
#define HSYMTMRIE	(1<<5)
#define SECIE		(1<<4)
#define RXIE		(1<<3)
#define TXG2IE		(1<<2)
#define TXG1IE		(1<<1)
#define TXNIE		(1)

/* SLPACK */
#define _SLPACK		(1<<7)
#define WAKECNT_L(x)	(x & 0x03F)

/* RFCTL */
#define WAKECNT_H(x)	((x & 0x03) << 3)
#define RFRST		(1<<2)
#define RFTXMODE	(1<<1)
#define RFRXMODE	(1)

/* REGISTERS */
#define MRF_RXMCR    0x00
#define MRF_PANIDL   0x01
#define MRF_PANIDH   0x02
#define MRF_SADRL    0x03
#define MRF_SADRH    0x04
#define MRF_RXFLUSH  0x0D
#define MRF_TXNCON   0x1B
#define MRF_PACON2   0x18
#define MRF_WAKECON  0x22
#define MRF_TXSTAT   0x24
#define MRF_SOFTRST  0x2A
#define MRF_TXSTBL   0x2E
#define MRF_INTSTAT  0x31
#define MRF_INTCON   0x32
#define MRF_GPIO     0x33
#define MRF_TRISGPIO 0x34
#define MRF_SLPACK   0x35
#define MRF_RFCTL    0x36
#define MRF_BBREG1   0x39
#define MRF_BBREG2   0x3A
#define MRF_BBREG6   0x3E
#define MRF_CCAEDTH  0x3F
#define MRF_RFCON0   0x200
#define MRF_RFCON1   0x201
#define MRF_RFCON2   0x202
#define MRF_RFCON3   0x203
#define MRF_RFCON6   0x206
#define MRF_RFCON7   0x207
#define MRF_RFCON8   0x208
#define MRF_RFSTATE  0x20F
#define MRF_RSSI     0x210
#define MRF_SLPCON0  0x211
#define MRF_SLPCON1  0x220
#define MRF_TESTMODE 0x22F

// Packet Transmission Details
//
// 802.15.4 MAC Data Frame (max size = max PSDU size = 127)
// -------------------------------------------------------------
// | MAC Header (MHR) | Data Payload (MSDU) | MAC Footer (MFR) |
// -------------------------------------------------------------
// |        9         |       1 - 116       |         2        |
// -------------------------------------------------------------
//
// 802.15.4 MAC Header (MHR)
// -------------------------------------------------------
// | Frame Control | Sequence Number | Addressing Fields |
// -------------------------------------------------------
// |       2       |        1        |         6         |
// -------------------------------------------------------
//
// 802.15.4 Addressing Fields (16-bit addressing with PAN ID)
// -------------------------------------------------
// | PAN ID | Destination Address | Source Address |
// -------------------------------------------------
// |    2   |          2          |        2       |
// -------------------------------------------------
//
// MRF24J40 TX FIFO
// ---------------------------------------------------------------------
// | Header Length | Frame Length | Header (MHR) | Data Payload (MSDU) |
// ---------------------------------------------------------------------
// |       1       |       1      |       9      |       1 - 116       |
// ---------------------------------------------------------------------

#define MRF_PAN_ID_SIZE 2
#define MRF_DEST_ADDR_SIZE 2
#define MRF_SRC_ADDR_SIZE 2
#define MRF_ADDR_FIELDS_SIZE (MRF_PAN_ID_SIZE + MRF_DEST_ADDR_SIZE + MRF_SRC_ADDR_SIZE)
#define MRF_MHR_SIZE (2 + 1 + MRF_ADDR_FIELDS_SIZE)
#define MRF_MFR_SIZE 2
#define MRF_MAX_PSDU_SIZE 127
#define MRF_MAX_PAYLOAD_SIZE (MRF_MAX_PSDU_SIZE - MRF_MHR_SIZE - MRF_MFR_SIZE)
#define MRF_MAX_TX_FIFO_SIZE (1 + 1 + MRF_MHR_SIZE + MRF_MAX_PAYLOAD_SIZE)

// Packet Reception Details
//
// MRF24J40 RX FIFO
// ------------------------------------------------------------------------
// | Frame Length | Header (MHR) | Data Payload (MSDU) | FCS | LQI | RSSI |
// ------------------------------------------------------------------------
// |       1      |       9      |       1 - 116       |  2  |  1  |   1  |
// ------------------------------------------------------------------------

#define MRF_MAX_RX_FIFO_SIZE (1 + MRF_MHR_SIZE + MRF_MAX_PAYLOAD_SIZE + 2 + 1 + 1)

enum Mrf24j40Type { MRF24J40MA, MRF24J40MB, MRF24J40MC, MRF24J40MD, MRF24J40ME };

class MRF
{
  public:

    MRF(Mrf24j40Type type, int cs = A3);
    void begin(void);
    void setPanId(uint16_t panId);
    void setAddress(uint16_t addr);
    void setCoordinator(bool coord);
    void setChannel(int channel);
    float measureSignalStrength();
    float getSignalStrength();
    int getLinkQuality();
    void sleep();
    void wakeup();

    bool write(uint8_t b);
    uint8_t read();
    bool writeInt(int16_t i);
    int16_t readInt();
    bool writeUnsignedInt(uint16_t u);
    uint16_t readUnsignedInt();
    bool writeLong(int32_t l);
    int32_t readLong();
    bool writeUnsignedLong(uint32_t u);
    uint32_t readUnsignedLong();
    bool writeFloat(float f);
    float readFloat();
    int writeBytes(uint8_t* buf, int size);
    int readBytes(uint8_t* buf, int size);
    int writeString(char* str);
    const char* readString(char* str = NULL, int size = 0);
    
    void startPacket();
    int bytesLeftToWrite();
    bool sendPacket(uint16_t addr, bool ack = false);
    bool transmissionDone();
    bool transmissionSuccess();
    bool receivePacketOnNoInt();
    int bytesLeftToRead();
	void RXcopy();

    uint8_t readShort(uint8_t address);
    void writeShort(uint8_t address, uint8_t data);
	void clear();
	void setPower(uint8_t percent);
	
	void clearISR();
	void enableRX(bool state);
	void flushRX();

  private:
    static SPISettings spiSettings;
  
    int cs;
    Mrf24j40Type type;
    uint16_t panId;
    uint16_t srcAddr;
    uint8_t seqNumber;
    int txCount;
    uint8_t txBuf[MRF_MAX_PAYLOAD_SIZE];
    int rxCount;
    int rxSize;
    uint8_t rxBuf[MRF_MAX_RX_FIFO_SIZE];
    uint8_t rssi;
    uint8_t lqi;
 
    void reset();
    void init();
    void enablePaLna();
    uint8_t readLong(uint16_t address);
    void writeLong(uint16_t address, uint8_t data);
    bool writeToBuffer(void* data, int size);
    bool readFromBuffer(void* data, int size);
};

#endif
