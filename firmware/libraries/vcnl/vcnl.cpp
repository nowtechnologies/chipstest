/*
    VCNL 4020 minimal library
	now technologies 2015;
*/

#include "Arduino.h"
#include "Wire.h"
#include "vcnl.h"

boolean sensor_found = false;
long wire_counter = 0;
long wire_timeout = 100000;

uint8_t VCNL::read8(uint8_t address)
{
   Wire.beginTransmission(VCNL4000_ADDRESS);
   Wire.write(address);  
   Wire.endTransmission();
   delayMicroseconds(170); //required
   Wire.requestFrom(VCNL4000_ADDRESS, 1);
   wire_counter = 0;
    while(!Wire.available()){
 	 wire_counter++;
	 if (wire_counter >= wire_timeout) return 0;
    }  
   return Wire.read();
}


uint16_t VCNL::read16(uint8_t address)
{
  uint16_t data;
  Wire.beginTransmission(VCNL4000_ADDRESS);
  Wire.write(address);
  Wire.endTransmission();

  Wire.requestFrom(VCNL4000_ADDRESS, 2);
  wire_counter = 0;
  while(!Wire.available()){
	wire_counter++;
	if (wire_counter >= wire_timeout) return 0;
  }  
  data = Wire.read();
  data <<= 8;
  while(!Wire.available());
  data |= Wire.read();

  return data;
}

void VCNL::write8(uint8_t address, uint8_t data)
{
  Wire.beginTransmission(VCNL4000_ADDRESS);
  Wire.write(address);
  Wire.write(data);  
  Wire.endTransmission();
}

void VCNL::initialize(int IR_POWER, int FREQ){
  uint8_t rev = read8(VCNL4000_PRODUCTID);
  
  if ((rev & 0xF0) != 0x20) {
	  sensor_found = false;
  }
  else{
  	  sensor_found = true;
  // set infrared led transmission power
  write8(VCNL4000_IRLED, IR_POWER);        
  // set samplerate
  write8(VCNL4000_SIGNALFREQ, FREQ);
  write8(VCNL4000_PROXINITYADJUST, 0x81);
  }
}

bool VCNL::getState(){
	return sensor_found;
}

uint8_t VCNL::getSamplerate() {
 if (sensor_found) return read8(VCNL4000_SIGNALFREQ);
 /*
 if (freq == VCNL4000_3M125) Serial.println("3.125 MHz");
 if (freq == VCNL4000_1M5625) Serial.println("1.5625 MHz");
 if (freq == VCNL4000_781K25) Serial.println("781.25 KHz");
 if (freq == VCNL4000_390K625) Serial.println("390.625 KHz");
 */
}

uint16_t VCNL::getProximity() {
if (sensor_found) {
  write8(VCNL4000_COMMAND, VCNL4000_MEASUREPROXIMITY);
  while (1) {
    uint8_t result = read8(VCNL4000_COMMAND);
    if (result & VCNL4000_PROXIMITYREADY) {
      return read16(VCNL4000_PROXIMITYDATA);
    }
  }
 }
}
