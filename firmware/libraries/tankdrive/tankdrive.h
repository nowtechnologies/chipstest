#ifndef TANKDRIVE_H
#define TANKDRIVE_H
#include "Arduino.h"

class Tank{
public:
  struct Powers{
    float left;
    float right;
  };
  Powers powers;

  struct Powers Drive(float throttle, float steer);
  void setSteeringPoint(float toThis);
  void setTurnFactor(float toThis);

};

#endif
