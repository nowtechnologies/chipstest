#include "tankdrive.h"

float steering_point = 1.0f;
float turn_factor = 0.8f;

void Tank::setSteeringPoint(float toThis){
    steering_point = toThis;
}

void Tank::setTurnFactor(float toThis){
    turn_factor = toThis;
}

Tank::Powers Tank::Drive(float x, float y){

  Powers powers;
  float Steer, Throttle;
  float MotorA, MotorB;

  Throttle = steering_point - abs(y / 1.0f); //  inverse magnitude
  float steer_modifier = Throttle * turn_factor;
  Steer = x / 1.0f * turn_factor;

  // Turn With Throttle
  float twt_MotorA = y * (steering_point + Steer);
  float twt_MotorB = y * (steering_point - Steer);

  // No Throttle Steering
  float nt_MotorA = x * Throttle;/// turn_factor;
  float nt_MotorB = -x * Throttle;/// turn_factor;

  // mixing
  MotorA = twt_MotorA + nt_MotorA;
  MotorB = twt_MotorB + nt_MotorB;

  powers.left  = constrain(MotorA, -1.0f, 1.0f);
  powers.right = constrain(MotorB, -1.0f, 1.0f);

  return powers;
}
