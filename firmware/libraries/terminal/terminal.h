#ifndef __TERMINAL_H__
#define __TERMINAL_H__

#include "config.h"
#include "Arduino.h"

#define HDR1 0x55
#define HDR2 0xAA

#define DONGLE_TYPE 16
#define HCP_TYPE 32
#define GRCP_TYPE 64
#define RADIO 8
#define HEADSET 4

#define typeIndex 2
#define modeIndex 3
#define lengthIndex 4
#define dataIndex 5
#define startIndex 2
#define serialIndex 0

#define TERM_CONN_PIN 32

class Terminal{
	// Constructors
	public:
		Terminal(Stream *serial);

	// Destructors
	public:
		~Terminal(void);

	// Public Methods
	public:

	struct Data {
		byte mode;
		byte length;
		byte list[16];
	};
	Data data;

	byte gotCommand();
	void initialize();
	void loopBack();
	void sendReport(byte toRequest, byte ser1, byte ser2, byte data1, byte data2);
	struct Data getData();
	void processed();
	void acknowledge(byte thisModeCommand, byte ser1, byte ser2, byte data1, byte data2);
	void setACK(bool toggle);
	void clearBuffer();
	bool connected();

private:
	uint16_t    m_Timeout;
	Stream      *m_Serial;

};

#endif
