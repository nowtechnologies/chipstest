
	#include "terminal.h"
	#include "config.h"
	#include "Arduino.h"

	extern unsigned int __bss_end;
	extern unsigned int __heap_start;
	extern void *__brkval;

	byte index = 0; // byte counter
 	byte buffer[33] = ""; // ringbuffer
	byte dataLength = 0;
	byte gotNewCommand = 0;
	bool ack_on = false;

	byte ack[]    = { HDR1, HDR2, DONGLE_TYPE, 0, 9, 0, 0, 0, 0 }; // shortest possible packet for ack
	byte report[] = { HDR1, HDR2, DONGLE_TYPE, 0, 9, 0, 0, 0, 0 }; // fixed sized report packet

	int freeMem(){
		int free_memory = 0;
		  if((int)__brkval == 0) {
		    free_memory = ((int)&free_memory) - ((int)&__bss_end);
		  } else {
		    free_memory = ((int)&free_memory) - ((int)__brkval);
		}
		return free_memory;
	};

///

Terminal::Terminal(Stream *serial) {
  m_Timeout = 100;
  m_Serial = serial;
}

Terminal::~Terminal(void) {
  m_Serial = NULL;
}

void Terminal::initialize(){
	pinMode(TERM_CONN_PIN, INPUT);
}

bool Terminal::connected(){
	if ( digitalRead(TERM_CONN_PIN) ) return true; else return false;
}
///

	void Terminal::clearBuffer(){
			this->m_Serial->flush();
			for (uint16_t i = 0; i <= sizeof(buffer)-1; i++){
				buffer[i] = 0;
			}
			index = 0;
	};
/*
	void Terminal::initialize(long baudrate){
		this->m_Serial->begin(baudrate);//Serial2.begin(DEFAULT_BAUDRATE);
		clearBuffer();
	};
*/
	void Terminal::loopBack(){

		while (m_Serial->available() > 0) {
	    m_Serial->write(m_Serial->read());
		}
	};

void Terminal::sendReport(byte toRequest, byte serial1, byte serial2,  byte data1, byte data2){
		report[modeIndex] = toRequest;
		report[dataIndex] = serial1;
		report[dataIndex+1] = serial2;
		report[dataIndex+2] = data1;
		report[dataIndex+3] = data2;

		for (uint16_t i = 0; i <= sizeof(report)-1; i++){
		 m_Serial->write(report[i]);
		}
	};

	struct Terminal::Data Terminal::getData(){
		return data;
	}

	void Terminal::processed(){
		gotNewCommand = 0;
	}

	byte Terminal::gotCommand(){

	    while (m_Serial->available() > 0){

        byte b = m_Serial->read();
//TODO: separate buffers
				buffer[index] = b;
        index++;

        delayMicroseconds(3200);

		    if (buffer[0] == HDR1 && buffer[1] == HDR2 && buffer[lengthIndex] == index){

					if ( (buffer[typeIndex] == DONGLE_TYPE) || (buffer[typeIndex] == HCP_TYPE) || (buffer[typeIndex] == GRCP_TYPE) ) {
						gotNewCommand = buffer[typeIndex];
						data.mode = buffer[modeIndex];
						dataLength = buffer[lengthIndex];
						data.length = dataLength - dataIndex;
						for (uint16_t c = dataIndex; c <= dataLength; c++){
							data.list[c - dataIndex] = buffer[c];
						}
					}
				}
				if (index == sizeof(buffer)-1) index = 0; // now it's a ringbuffer
		}

	    for (uint16_t i = 0; i <= sizeof(buffer)-1; i++){
	      if ( buffer[i] == HDR1 && buffer[i+1] == HDR2 ) clearBuffer();
	    }

		return gotNewCommand;
	};

	void Terminal::acknowledge(byte toRequest, byte serial1, byte serial2,  byte data1, byte data2){
		if (ack_on){

			ack[modeIndex]   = toRequest;
			ack[dataIndex]   = serial1;
			ack[dataIndex+1] = serial2;
			ack[dataIndex+2] = data1;
			ack[dataIndex+3] = data2;

			for (uint16_t i = 0; i <= sizeof(ack)-1; i++){
				m_Serial->write(ack[i]);
			}
		}
	}

	void Terminal::setACK(bool state){
		ack_on = state;
	}
