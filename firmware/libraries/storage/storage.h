#ifndef __STORAGE_H__
#define __STORAGE_H__

	/*

	Use only the first 512 bytes for compatibility with smaller AVRs.

	*/

	#include "Arduino.h"
	#include "EEPROM.h"

	  //////////////////////////////////////////////////////////////////////////////////////
	 //					  			EEPROM ADDRESS TABLE								 //
	//////////////////////////////////////////////////////////////////////////////////////

	#define ROM_TXPOWER  6

	#define ROM_ACCEL_X  8		// 8  | 9
	#define ROM_ACCEL_Y  10		// 10 | 11
	#define ROM_ACCEL_Z  12		// 12 | 13
	#define ROM_GYRO_X   14		// 14 | 15
	#define ROM_GYRO_Y   16		// 16 | 17
	#define ROM_GYRO_Z   18		// 18 | 19
	#define ROM_MAG_X 	 20
	#define ROM_MAG_Y 	 22
	#define ROM_MAG_Z 	 24
	#define ROM_ACCEL_RAD    26
	#define ROM_MAG_RAD			 28  // 28 | 29

	#define ROM_FIRSTDEV 30
	#define ROM_LASTDEV  31

	#define ROM_PAN      32 // 32 - 33

	#define ROM_INIT     64
	#define ROM_CHECK    65
	#define ROM_SUM		 66

	class Storage {

	public:

		void initialize();
		void resetConfig();
		void factoryReset();

		void storeFirstDevice(byte thisByte);
		void storeLastDevice(byte thisByte);
		void storePAN(int thisValue);
		void storeTXpower(byte thisByte);

		void storeAX(int offset);
		void storeAY(int offset);
		void storeAZ(int offset);
		void storeGX(int offset);
		void storeGY(int offset);
		void storeGZ(int offset);
		void storeMX(int offset);
		void storeMY(int offset);
		void storeMZ(int offset);
		void storeAccelRad(int radius);
		void storeMagRad(int radius);

		int16_t accel_offset_x();
		int16_t accel_offset_y();
		int16_t accel_offset_z();
		int16_t gyro_offset_x();
		int16_t gyro_offset_y();
		int16_t gyro_offset_z();
		int16_t mag_offset_x();
		int16_t mag_offset_y();
		int16_t mag_offset_z();
		int16_t accel_radius();
		int16_t mag_radius();

		bool loadCalibration(uint8_t* toThisArray);
		int  PAN();
		byte lastDevice();
		byte firstDevice();
		byte TXpower();

		int16_t  readInt(byte startAddress);

	private:

		bool compareBytes(byte thisbyte, byte onaddress);
		void writeInt(int thisInt, byte startAddress);
		void writeByte(byte thisByte, byte startAddress);
		byte readByte(byte onAddress);

	};

#endif
