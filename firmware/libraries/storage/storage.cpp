  #include "Arduino.h"
	#include "EEPROM.h"
	#include "storage.h"
	#include "config.h"


	///////////////////////////////////////////////////////////
	//					  	  PRIVATE						 //
	///////////////////////////////////////////////////////////

	bool Storage::compareBytes(byte thisbyte, byte onaddress){
		bool answer = false;
		byte storedValue = EEPROM.read(onaddress);
		if (thisbyte == storedValue) answer = true;
		return answer;
	};

	int16_t Storage::readInt(byte startAddress){
  		int16_t value = word(EEPROM.read(startAddress), EEPROM.read(startAddress + 1)) - 65536;
		return  value;
	};

	void Storage::writeInt(int thisInt, byte startAddress){
    	if (Storage::readInt(startAddress) != thisInt){
			EEPROM.write(startAddress, highByte(thisInt));
			EEPROM.write(startAddress + 1, lowByte(thisInt));
    	}
	};

  	byte Storage::readByte(byte onAddress){
		return  EEPROM.read(onAddress);
	};

	void Storage::writeByte(byte thisByte, byte startAddress){
		if ( !Storage::compareBytes(thisByte, startAddress) ) EEPROM.write(startAddress, thisByte);
	};



	///////////////////////////////////////////////////////////
	//					  	  PUBLIC						 //
	///////////////////////////////////////////////////////////

	void Storage::storePAN(int thisValue){
		Storage::writeInt(thisValue, ROM_PAN);
	};

	void Storage::storeLastDevice(byte thisByte){
		Storage::writeByte(thisByte, ROM_LASTDEV);
	};

	void Storage::storeFirstDevice(byte thisByte){
		Storage::writeByte(thisByte, ROM_FIRSTDEV);
	};

	void Storage::storeTXpower(byte thisByte){
		Storage::writeByte(thisByte, ROM_TXPOWER);
	}

	void Storage::storeAX(int thisValue){
		Storage::writeInt(thisValue, ROM_ACCEL_X);
	};

	void Storage::storeAY(int thisValue){
		Storage::writeInt(thisValue, ROM_ACCEL_Z);
	};

	void Storage::storeAZ(int thisValue){
		Storage::writeInt(thisValue, ROM_ACCEL_Z);
	};

	void Storage::storeMX(int thisValue){
		Storage::writeInt(thisValue, ROM_MAG_X);
	};

	void Storage::storeMY(int thisValue){
		Storage::writeInt(thisValue, ROM_MAG_Y);
	};

	void Storage::storeMZ(int thisValue){
		Storage::writeInt(thisValue, ROM_MAG_Z);
	};

	void Storage::storeGX(int thisValue){
		Storage::writeInt(thisValue, ROM_GYRO_X);
	};

	void Storage::storeGY(int thisValue){
		Storage::writeInt(thisValue, ROM_GYRO_Y);
	};

	void Storage::storeGZ(int thisValue){
		Storage::writeInt(thisValue, ROM_GYRO_Z);
	};

	void Storage::storeAccelRad(int thisValue){
		Storage::writeInt(thisValue, ROM_ACCEL_RAD);
	};

	void Storage::storeMagRad(int thisValue){
		Storage::writeInt(thisValue, ROM_MAG_RAD);
	};

  int16_t Storage::accel_offset_x(){
		int16_t value = Storage::readInt(ROM_ACCEL_X);
		return value;
	};

  int16_t Storage::accel_offset_y(){
		int16_t value = Storage::readInt(ROM_ACCEL_Y);
		return value;
	};

  int16_t Storage::accel_offset_z(){
		int16_t value = Storage::readInt(ROM_ACCEL_Z);
		return value;
	};

  int16_t Storage::gyro_offset_x(){
    int16_t value = Storage::readInt(ROM_GYRO_X);
    return value;
  };

  int16_t Storage::gyro_offset_y(){
    int16_t value = Storage::readInt(ROM_GYRO_Y);
    return value;
  };

  int16_t Storage::gyro_offset_z(){
    int16_t value = Storage::readInt(ROM_GYRO_Z);
    return value;
  };

  int16_t Storage::mag_offset_x(){
    int16_t value = Storage::readInt(ROM_MAG_X);
    return value;
  };

  int16_t Storage::mag_offset_y(){
    int16_t value = Storage::readInt(ROM_MAG_Y);
    return value;
  };

  int16_t Storage::mag_offset_z(){
    int16_t value = Storage::readInt(ROM_MAG_Z);
    return value;
  };

  int16_t Storage::accel_radius(){
    int16_t value = Storage::readInt(ROM_ACCEL_RAD);
    return value;
  };

  int16_t Storage::mag_radius(){
    int16_t value = Storage::readInt(ROM_MAG_RAD);
    return value;
  };

	int Storage::PAN(){
		int value = Storage::readInt(ROM_PAN);
		return value;
	};

	byte Storage::lastDevice(){
		byte value =  Storage::readByte(ROM_LASTDEV);
		return value;
	};

	byte Storage::firstDevice(){
		byte value =  Storage::readByte(ROM_FIRSTDEV);
		return value;
	};

	byte Storage::TXpower(){
		byte value =  Storage::readByte(ROM_TXPOWER);
		return value;
	}

bool Storage::loadCalibration(uint8_t *toThisArray){

  for (int i = ROM_ACCEL_X; i <= ROM_MAG_RAD+1; i++){
      toThisArray[i] = EEPROM.read(i);
  }
  return true;
}

	/*
	int  Storage::Offset(byte thisOffset){
		int value = Storage::readInt(OFFSETADDRESS[thisOffset]);
		return value;
	};
	*/

	///////////////////////////////////////////////////////////
	//					  	 INITIALIZE						 //
	///////////////////////////////////////////////////////////

	Storage Default;

	void storeDefaults(){
		Default.storePAN(DEFAULT_PAN);
		Default.storeFirstDevice(DEFAULT_FIRSTDEV);
		Default.storeLastDevice(DEFAULT_LASTDEV);
		Default.storeTXpower(DEFAULT_TXPOWER);
		Default.storeAX(0);
		Default.storeAY(0);
		Default.storeAZ(0);
		Default.storeMX(0);
		Default.storeMY(0);
		Default.storeMZ(0);
		Default.storeGX(0);
		Default.storeGY(0);
		Default.storeGZ(0);
		Default.storeAccelRad(0);
		Default.storeMagRad(0);
	};

	void factoryReset(){
		storeDefaults(); // MEH...
	};

	void Storage::initialize(){

		Serial.print("CFG : ");

		byte init  = Storage::readByte(ROM_INIT);
		byte check = Storage::readByte(ROM_CHECK);
		byte checksum = init + check;

		if ( !Storage::compareBytes(checksum, ROM_SUM) ) {
			EEPROM.write(ROM_INIT,  55);
			EEPROM.write(ROM_CHECK, 55);
			EEPROM.write(ROM_SUM,  110);
			factoryReset();
			Serial.println("RESET");
		}
		else {
			Serial.println("OK");
		}
	};

	void Storage::resetConfig(){
		storeDefaults();
	};

	void Storage::factoryReset(){
		factoryReset();
	};
