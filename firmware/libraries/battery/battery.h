#ifndef __BATTERY_H__
#define __BATTERY_H__
	
	#define HEADSET_BATTERY_STATE_PIN 0
	
	#include "Arduino.h"

	class Battery {
		public: 
			uint16_t voltage();
	};
	
#endif
