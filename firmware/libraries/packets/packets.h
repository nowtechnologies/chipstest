#ifndef __PACKETS_H__
#define __PACKETS_H__
#include "Arduino.h"

// NETWORK
#define HEADSET_ADDRESS    1
#define WHEELCHAIR_ADDRESS 2
#define ANDROID_ADDRESS    3
#define DONGLE_ADDRESS     4

// PACKET TYPE IDENTIFIERS
#define PID_SHP     1
#define PID_RAW     2
#define PID_HCP     32
#define PID_GRCP    64

// COMMAND MODES
/*
. remote control (UI: no fixed center for virtual joy)
. head-drive config. angle to axis mapping (tilt/rotate/extremes)
. set stream mask for message type filtering (subscribe/unsubscribe)
. assign mask patterns to predefined variables [1.1.1.1.1.1.1.0] = ERRORLEVEL
*/

typedef struct {
	byte functionActive;
	byte deviceToControl;
	byte firstDevice;
	byte lastDevice;
	byte imuStatus;
	byte magnetoStatus;
	byte gyroStatus;
	byte acceleroStatus;
	float w;
	float x;
	float y;
	float z;
	uint16_t infrared;
	uint16_t battery;
	int temperature;
} radioSHP;

typedef struct {
	byte id;
	int  value;
} radioHCP;

  ////////////////////
 // LEGACY PACKETS //
////////////////////

typedef struct {
  byte lead; // 0
  byte in;  // 1
  byte type; // 2
  byte mode; // 3
  byte length; // 4
  int serial; // 5|6
} HEADER; //len 7

// v.1.0 standard headset packet
typedef struct {
  HEADER header;
  // DATA
  byte BETA; // 7 // magnetometer mix percentage = gyebroBeta(1.0f);
  byte MAGS; // 8
  float w, x, y, z; // 9 ->
  uint16_t blink;
  uint16_t battery;
  int16_t tilt;
  byte controller;
  byte wheelchair;
} uartSHP;

typedef struct {
  HEADER header;
  // DATA
  byte BETA; // beta
  byte TRESH; // gyro treshold
  uint16_t GX, GY, GZ, MX, MY, MZ; // offset matrix
} uartHCP;

#endif
