#include "sound.h"

// PRIVATE

unsigned long _sound_time; // Used to track end note with timer when playing note in the background.

void Sound::silent() {
  TIMSK1 &= ~_BV(OCIE1A);         
  TCCR1B  = _BV(CS11);            
  TCCR1A  = _BV(WGM10);          
  PWMT1PORT &= ~_BV(PWMT1AMASK); 
  PWMT1PORT &= ~_BV(PWMT1BMASK);  
};

ISR(TIMER1_COMPA_vect) { 		// Timer interrupt vector.
  if (millis() >= _sound_time) {
	  TIMSK1 &= ~_BV(OCIE1A);         
	  TCCR1B  = _BV(CS11);            
	  TCCR1A  = _BV(WGM10);          
	  PWMT1PORT &= ~_BV(PWMT1AMASK); 
	  PWMT1PORT &= ~_BV(PWMT1BMASK);  
  }
};

void Sound::loud(unsigned long frequency, unsigned long length, unsigned long silence) {

  if (frequency == 0) { silent(); return; }                // If frequency is 0, turn off sound and return.
  
  PWMT1DREG |= _BV(PWMT1AMASK) | _BV(PWMT1BMASK); // Set timer 1 PWM pins to OUTPUT (because analogWrite does it too).

  uint8_t prescaler = _BV(CS10);                 // Try using prescaler 1 first.
  unsigned long top = F_CPU / frequency / 2 - 1; // Calculate the top.
  if (top > 65535) {                             // If not in the range for prescaler 1, use prescaler 256 (122 Hz and lower @ 16 MHz).
    prescaler = _BV(CS12);                       // Set the 256 prescaler bit.
    top = top / 256 - 1;                         // Calculate the top using prescaler 256.
  }
  unsigned int duty = top >> 1;                      // 50% duty cycle (loudest and highest quality).
  if (length > 0) {                // Background tone playing, returns control to your sketch.

    _sound_time = millis() + length; // Set when the note should end.
    TIMSK1 |= _BV(OCIE1A);         // Activate the timer interrupt.
  }

  ICR1   = top;                         // Set the top.
  if (TCNT1 > top) TCNT1 = top;         // Counter over the top, put within range.
  TCCR1B = _BV(WGM13)  | prescaler;     // Set PWM, phase and frequency corrected (top=ICR1) and prescaler.
  OCR1A  = OCR1B = duty;                // Set the duty cycle (volume).
  TCCR1A = _BV(COM1A1) | _BV(COM1B1) | _BV(COM1B0); // Inverted/non-inverted mode (AC).
  
  delay(length+silence);

};

void Sound::normal(unsigned long frequency, unsigned long length, unsigned long silence){
	tone(9, frequency, length);
	delay(length+silence);
}

// PUBLIC

void Sound::startup(){
	normal(NOTE_E4, 50, 50);
	loud(NOTE_E4, 50, 50);
	loud(NOTE_E4, 100, 50);
	loud(NOTE_B4, 400, 0);
};

void Sound::controlChange(byte to){
	for (int i = 1; i < to; i++ ){
		loud(NOTE_B4, 100, 50);
	}
};

void Sound::firstDevice(byte to){
	for (int i = 1; i < to; i++ ){
		loud(NOTE_E4, 100, 50);
	}
};

void Sound::functionActive(bool on){

	if (on){
		loud(NOTE_E4, 50, 50);
		loud(NOTE_B4, 100, 0);	
	}
	else {
		loud(NOTE_B4, 50, 50);
		loud(NOTE_E4, 100, 0);
	}
};

void Sound::romErased(){
	normal(NOTE_B4, 100, 0);
	loud(NOTE_B4, 100, 0);
};

void Sound::Error(){
	normal(NOTE_E4, 100, 0);
	delay(400);
	//loud(NOTE_C5, 100, 0);
};

void Sound::acknowledged(){
	loud(NOTE_F4, 50, 0);	
	delay(50);
	loud(NOTE_F4, 50, 0);	
}

void Sound::alert(){ 
  normal(NOTE_E5, 50, 0);   
}