
	#include "Wire.h"
	#include "config.h"
	#include "terminal.h"
	#include "timer.h"
	#include "onebutton.h"
	#include "MRF24J.h"
	#include "EEPROM.h"
	#include "storage.h"
	#include "RobotEQ.h"
	#include "Adafruit_Sensor.h"
	#include "Adafruit_BNO055.h"
	#include "utility/imumaths.h"
	#include "sound.h"
	#include "packets.h"
	#include "glory.h"
	#include "tankdrive.h"

	Terminal  bluetooth(&Serial2);
	RobotEQ		chair(&Serial3);

	oneButton button;
	Storage		config;
	//Sound 		sound;
	Timer			timer;
	Tank			tank;
	Glory  		glory;

	Tank::Powers powers;
	Glory::Eulers head_null;
	Glory::Eulers head;
	Glory::Eulers platform_null;
	Glory::Eulers platform;
	Glory::Eulers control;

	int powerConst = 1000;

	MRF mrf(MRF24J40MA, MRF_CHIPSELECT);
	radioSHP headset;
	radioHCP headsetCommand;

	Adafruit_BNO055 bno = Adafruit_BNO055(55);

	enum BnoState {
		mode_config,
		mode_9dof,
		mode_6dof
	};
	BnoState bnoState = mode_9dof;

	enum BnoCalibLoc {
		loc_accel = 0,
		loc_gyro,
		loc_mag
	};

	bool bnoCalibration[3];
	Adafruit_BNO055::adafruit_bno055_rev_info_t bnoRevInfo;
	adafruit_bno055_offsets_t bnoOffsets;
	uint8_t tempCal[22];

  bool mutex = true;

/// GLOBAL VARIABLES /////////////////////////////////////////////////////////

  boolean controlEnabled  = false;
	boolean functionActive  = false;
	byte    deviceToControl = 4;
	byte    firstDevice     = 4;
	byte    lastDevice      = 5;
	byte    headsetMode     = FUSED_MODE; // by default
	byte    activity        = 0;

	uint8_t packetID;
	uint8_t rxBuffer[sizeof(headset)];
	volatile bool commandReceived = false;

	uint8_t imuStatus, gyroStatus, acceleroStatus, magnetoStatus = 0;
	int8_t temperature;
	imu::Quaternion q;

	bool imu_installed = false;
	bool ledState = false;

	byte hw_error = 0;

/// POST /////////////////////////////////////////////////////////////////////

    void registerError(byte thisType, bool state){
	   bitWrite(hw_error, thisType, state);
    }

/// CONFIGURATION ///////////////////////////////////////////////////////////

	 void loadOffsets(){

		bnoOffsets.accel_offset_x = config.accel_offset_x();
		bnoOffsets.accel_offset_y = config.accel_offset_y();
		bnoOffsets.accel_offset_z = config.accel_offset_z();
		bnoOffsets.accel_radius   = config.accel_radius();

		bnoOffsets.gyro_offset_x = config.gyro_offset_x();
		bnoOffsets.gyro_offset_y = config.gyro_offset_y();
		bnoOffsets.gyro_offset_z = config.gyro_offset_z();

		bnoOffsets.mag_offset_x = config.mag_offset_x();
		bnoOffsets.mag_offset_y = config.mag_offset_y();
		bnoOffsets.mag_offset_z = config.mag_offset_z();
		bnoOffsets.mag_radius 	= config.mag_radius();

		bno.setSensorOffsets(bnoOffsets);

	 }

   void loadConfig(){

	   loadOffsets();

   }

	 /// I2C //////////////////////////////////////////////////////////////////////

	void I2Cinit(){

		Wire.begin();

	    byte error, address;
	    int nDevices;

	    Serial.print("I2C : ");

	    nDevices = 0;
	    for(address = 1; address < 127; address++ )
	    {
		  Wire.beginTransmission(address);
	      error = Wire.endTransmission();

	      if (error == 0)
	      {
	        if (address<16)
	          Serial.print("0");
	          Serial.print(address,HEX);
	          Serial.print("! ");
	        nDevices++;
	      }
	      else if (error==4)
	      {
	        Serial.print("Unknow error at address 0x");
	        if (address<16)
	          Serial.print("0");
	          Serial.println(address,HEX);
	      }
	    }
	    if (nDevices == 0) {
			Serial.println("NO DEVICE");
			registerError(I2C_ERROR, true);
		}
		else {
			registerError(I2C_ERROR, false);
			Serial.println();
		}

	}

/// RADIO //////////////////////////////////////////////////////////////////

void rxInterruptHandler() {
	mrf.clearISR();
	mrf.enableRX(false);
	mrf.RXcopy();
	packetID = mrf.read();
	mrf.readBytes(rxBuffer, sizeof(headset));
	memcpy(&headset, &rxBuffer, sizeof(headset));
	mrf.flushRX();
	mrf.enableRX(true);
	commandReceived = true;
}

	void MRFinit(){
		mrf.begin();
		mrf.setPanId(42);
		mrf.setChannel(DEFAULT_CHANNEL);
		mrf.setAddress(WHEELCHAIR_ADDRESS);
		pinMode(2, INPUT_PULLUP);
		attachInterrupt(0, rxInterruptHandler, FALLING);
		mrf.readShort(MRF_INTSTAT);
		// TODO: read something to figure out if MRF is working properly
		registerError(MRF_ERROR, false);
	}

	void loadRadio () {

	  if ( commandReceived ) {

   		switch (packetID){

  			case PID_SHP:

				/*
				headset.deviceToControl;
				headset.functionActive;
				headset.imuStatus;
				headset.magnetoStatus;
				headset.battery;
				headset.firstDevice;
				headset.lastDevice;

				headset.w;
				headset.x;
				headset.y;
				headset.z;
				headset.infrared;
				*/

				head = glory.getEuler(headset.w, headset.x, headset.y, headset.z);
				functionActive = bool(headset.functionActive - 1);
/*
				if ( headset.deviceToControl == WHEELCHAIR_ADDRESS && headset.functionActive > 0 ) {
					controlEnabled = bool(headset.functionActive - 1);
				}
*/
				break;
  		}

			commandReceived = false;
  	}
	}




	void transmit( byte mode ){

			switch (mode) {
/*
				case FUSED_MODE:
					shp.functionActive  = activity;
					shp.deviceToControl = deviceToControl;
					shp.firstDevice     = firstDevice;
					shp.lastDevice      = lastDevice;
					shp.imuStatus       = imuStatus;
					shp.magnetoStatus   = magnetoStatus;
					shp.gyroStatus      = gyroStatus;
					shp.acceleroStatus  = acceleroStatus;
					shp.w		    			= q.w();
					shp.x		    			= q.x();
					shp.y		    			= q.y();
					shp.z		    		  = q.z();
					shp.infrared	    = infrared;
					shp.battery	    	= battery.voltage();
					shp.temperature	  = temperature;

					uint8_t transmission[sizeof(shp)];
					memcpy(transmission, &shp, sizeof(shp));

					mrf.startPacket();
					mrf.write(PID_SHP); // initialize packet with identifier byte
					mrf.writeBytes(transmission, sizeof(shp)); // load structure to tx buffer
					mrf.sendPacket(0xFFFF); // broadcast without ack
				break;
				*/
			}

		//while ( !mrf.transmissionDone() ){}
	}

/// POWERMODULE ///////////////////////////////////////////////////////////////
bool chair_connected = false;

void chairInit(){

	chair.powerOff(false);
	delay(250);

	int retryCounter = 0;
	bool connect_success = true;
	Serial.print("Connecting to chair...");
	while (chair.isConnected() != ROBOTEQ_OK){

		Serial.print(".");

		if (retryCounter > 3) {
			connect_success = false;
			Serial.println("not found.");
			break;
		}

		delay(100);
		retryCounter++;
	}

	if (connect_success){
		Serial.println("success.");
		chair.commandMotorPower(1, 0);
		chair.commandMotorPower(2, 0);
		chair_connected = true;
	} else {
		chair_connected = false;
	}
}

/// SENSORS //////////////////////////////////////////////////////////////////

	void IMUinit(){

		bnoCalibration[loc_accel] = false;
    bnoCalibration[loc_gyro]  = false;
    bnoCalibration[loc_mag]   = false;

		if(!bno.begin(Adafruit_BNO055::OPERATION_MODE_NDOF)) {
			Serial.println("IMU : ERROR");
			registerError(IMU_ERROR, true);
			imu_installed = false;
		}
		else {
			bno.setExtCrystalUse(true);
			Serial.println("IMU : OK");
			imu_installed = true;
			registerError(IMU_ERROR, false);
		}
	}

	void getSensorData(){

				if (imu_installed){

					q = bno.getQuat();
					platform = glory.getEuler(q.w(), q.x(), q.y(), q.z());

					temperature = bno.getTemp();

					bno.getCalibration(&imuStatus, &gyroStatus, &acceleroStatus, &magnetoStatus);

					if (magnetoStatus  == 3 && !bnoCalibration[loc_mag] && !functionActive) {
						// Obtain magneto offsets
						bnoCalibration[loc_mag]   = true;
						bno.getSensorOffsets(bnoOffsets);
						config.storeMX(bnoOffsets.mag_offset_x);
						config.storeMY(bnoOffsets.mag_offset_y);
						config.storeMZ(bnoOffsets.mag_offset_z);
						config.storeMagRad(bnoOffsets.mag_radius);
						Serial.println("Local Mag calibrated");
						//sound.alert();
					}
					if (acceleroStatus == 3 && !bnoCalibration[loc_accel]  && !functionActive) {
						// Obtain accelero offsets
						bnoCalibration[loc_accel] = true;
						bno.getSensorOffsets(bnoOffsets);
						config.storeAX(bnoOffsets.accel_offset_x);
						config.storeAY(bnoOffsets.accel_offset_y);
						config.storeAZ(bnoOffsets.accel_offset_z);
					  config.storeAccelRad(bnoOffsets.accel_radius);
						Serial.println("Local Accelero calibrated");
						//sound.alert();
					}

					if (gyroStatus == 3 && !bnoCalibration[loc_gyro] && !functionActive) {
						// Obtain gyro offsets
						bnoCalibration[loc_gyro]  = true;
						bno.getSensorOffsets(bnoOffsets);
						config.storeGX(bnoOffsets.gyro_offset_x);
						config.storeGY(bnoOffsets.gyro_offset_y);
						config.storeGZ(bnoOffsets.gyro_offset_z);
						Serial.println("Local Gyro calibrated");
						//sound.alert();
					}

				}
	}


////SETUP///////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////SETUP///////

void resetConfigOnButtonHold(){
	int resetCounter = 0;
	while ( digitalRead(BUTTON_PIN) == 0 ) {
		resetCounter++;
		delay(1);
		if ( resetCounter  > 5000 ) {
			config.resetConfig();
			//sound.romErased();
		}
	}
}

	void haltAndCatchFire(){
		bool noGo = false;
		if (bitRead(hw_error,IMU_ERROR)) noGo = true;
			while(noGo) {
				//sound.Error();
				Serial.print("ERR : ");
				Serial.println(hw_error);
			}
	}

void setup() {

	Serial.begin(DEFAULT_BAUDRATE);
	Serial2.begin(DEFAULT_BAUDRATE);
	Serial3.begin(DEFAULT_BAUDRATE);

	bluetooth.initialize();
	config.initialize();

	//button.initialize();
	//resetConfigOnButtonHold();

	I2Cinit();
	IMUinit();
	MRFinit();
	loadConfig();
	chairInit();

	//haltAndCatchFire();
 	//sound.startup();
	pinMode(LED_PIN, OUTPUT);
	digitalWrite(LED_PIN, LOW);
}

/// MAIN ////////////////////////////////////////////////////////////////////

	bool lastActivity;
	void functionEvents(){

		if ((lastActivity != functionActive) && functionActive){
			bno.setMode(Adafruit_BNO055::OPERATION_MODE_NDOF_FMC_OFF);
			bnoCalibration[loc_accel] = false;
			bnoCalibration[loc_gyro]  = false;
			bnoCalibration[loc_mag]   = false;
			Serial.println("FMC OFF");
			// null positions
			head_null = head;
			platform_null = platform;
			chair.powerOff(false);
			// TODO: beep and delay
		}
		else if((lastActivity != functionActive) && !functionActive) {
			chair.powerOff(true);
			digitalWrite(LED_PIN, LOW);
			bno.setMode(Adafruit_BNO055::OPERATION_MODE_NDOF);
			Serial.println("FMC ON");
		}
		lastActivity = functionActive;
	}

   void loop() {

		 	loadRadio();
			getSensorData();
			functionEvents();

			if (functionActive && chair_connected){

				control = glory.getDifference(glory.substract(head, head_null) , glory.substract(platform, platform_null));
/*
				if (timer.poll(100)) {
					Serial.println(head.Phi-head_null.Phi);
					Serial.println(platform.Phi-platform_null.Phi);
					Serial.println(control.Phi);
					Serial.println("-----------");
				}
*/
				powers = tank.Drive(-control.Phi, -control.Psi/2);
				chair.commandMotorPower(1, int(glory.mapfloat(powers.left , -1.0f, 1.0f, -powerConst, powerConst)));
				chair.commandMotorPower(2, int(glory.mapfloat(powers.right, -1.0f, 1.0f, -powerConst, powerConst)));
			}

   }

////THE END/////////////////////////////////////////////////////////////////
