// DEBUG SWITCHES
//#define UART_MODE 3
//#define POWERSAVE

// VERSION
#define MAIN_VERSION      1
#define SUB_VERSION       2

// HARDWARE
#define BATTERY_PIN 	    0
#define BUTTON_PIN 			  8
#define LED_PIN				    13
#define MRF_WAKEPIN			  6
#define MRF_RESET			    4
#define MRF_CHIPSELECT    7
#define IR_PWR 				    20 // IR_POWER * 10 = 200mA, FREQ = 3 -> 390KHz lowest
#define IR_FRQ 				    3
#define SHORTPRESS 			  100

// HARDWARE ERROR CODEMASK
#define I2C_ERROR			    3 // 8
#define VCNL_ERROR			  1 // 2
#define IMU_ERROR			    2 // 4
#define MRF_ERROR			    0 // 1

// MPU MODES
#define ERROR_MODE	   		0
#define FUSED_MODE	   		1
#define RAW_MODE          2

// DEFAULTS
#define DEFAULT_CHANNEL     11
#define DEFAULT_PAN	   	    0
#define DEFAULT_FIRSTDEV    4
#define DEFAULT_LASTDEV     5
#define DEFAULT_TXPOWER     100

// TERMINAL
#define DEFAULT_BAUDRATE   9600
