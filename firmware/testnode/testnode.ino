
#define INT_MODE

#include "MRF24J.h"
#include "Timer.h"
#include "packets.h"

MRF mrf(MRF24J40MA, 7);
SHP shp;
Timer timer;

uint8_t packetID;
uint8_t rxBuffer[sizeof(shp)];
volatile bool gotPacket = false;

void printPacket(){
	Serial.print("ACT: "); Serial.println(shp.functionActive);
	Serial.print("DTC: "); Serial.println(shp.deviceToControl);
	Serial.print("FTD: "); Serial.println(shp.firstDevice);
	Serial.print("LTD: "); Serial.println(shp.lastDevice);
	Serial.print("IMU: "); Serial.println(shp.imuStatus);
	Serial.print("MAG: "); Serial.println(shp.magnetoStatus);
	Serial.print("GYR: "); Serial.println(shp.gyroStatus);
	Serial.print("ACC: "); Serial.println(shp.acceleroStatus);
	Serial.print("Q.W: "); Serial.println(shp.w);
	Serial.print("Q.X: "); Serial.println(shp.x);
	Serial.print("Q.Y: "); Serial.println(shp.y);
	Serial.print("Q.Z: "); Serial.println(shp.z);
	Serial.print("IRD: "); Serial.println(shp.infrared);
	Serial.print("BAT: "); Serial.println(shp.battery);
	Serial.print("TEM: "); Serial.println(shp.temperature);
	Serial.println("-----");
}

void getPacket(){
	packetID = mrf.read();  
	mrf.readBytes(rxBuffer, sizeof(shp));
	memcpy(&shp, &rxBuffer, sizeof(shp));
}

#ifdef INT_MODE
void rxInterruptHandler() {
	mrf.clearISR();
	mrf.enableRX(false);
			
	mrf.RXcopy();
	getPacket();
	
	mrf.flushRX();
	mrf.enableRX(true);
	gotPacket = true;
}
#endif


void setup() {	
	Serial.begin(9600);
	mrf.begin();
	mrf.setAddress(2);
#ifdef INT_MODE
	pinMode(2, INPUT_PULLUP);
	attachInterrupt(0, rxInterruptHandler, FALLING);
	mrf.readShort(MRF_INTSTAT);
#endif
}


int sec;

void loop() {
#ifndef INT_MODE
  if (mrf.receivePacket()) {
	  getPacket();
	  printPacket();
  }
#endif
#ifdef INT_MODE
  if (timer.poll(1000)) {
	  mrf.clear();
	  Serial.println(sec++); 
  }
  if (gotPacket){
	  printPacket();
	  gotPacket = false;
  }
#endif 
}

