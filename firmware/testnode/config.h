// VERSION
#define MAIN_VERSION       2
#define SUB_VERSION        0

// HARDWARE
#define IR_PWR 20 // IR_POWER * 10 = 200mA, FREQ = 3 -> 390KHz lowest
#define IR_FRQ 3
#define HEADSET_BATTERY_STATE_PIN 0
#define HEADSET_BUTTON_PIN 9
#define SHORTPRESS 100
#define MRF_WAKE 10

// NETWORK
#define HEADSET_ADDRESS    1
#define WHEELCHAIR_ADDRESS 2
#define ANDROID_ADDRESS    3
#define DONGLE_ADDRESS     4

// DEFAULTS
#define DEFAULT_CHANNEL    11
#define DEFAULT_PAN	       0
#define DEFAULT_FIRSTDEV   4 
#define DEFAULT_LASTDEV    5
	
// TERMINAL
#define SERIAL_TERMINAL_BAUD 9600

// HEADSET MODES
#define ERROR_MODE		   0
#define FUSED_MODE		   1
#define RAW_MODE           2
//#define UART_MODE 3
