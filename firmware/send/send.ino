#include <SPI.h>
#include <MRF24J.h>

MRF mrf(MRF24J40MA, 7);
byte b = 1;
int i = -2015;
unsigned int ui = 2015;
long l = -2015000;
unsigned long ul = 2015000;
float f = 2.015;
char* str = "Circuitar";
byte buf[3] = { 0x01, 0x02, 0x03 };

byte retry = 0;

void setup() {
	Serial.begin(9600);
	mrf.begin();
    mrf.setPanId(0);  
    mrf.setChannel(11);                        
	mrf.setAddress(1); // Network address
}

void loop() {
	// Write packet data
	mrf.startPacket();
  mrf.write(b++);
  for (int i = 0; i < 40; ++i) {
    mrf.write(b);
  }
	mrf.write(b++);
	mrf.writeInt(i);
	mrf.writeUnsignedInt(ui);
	mrf.writeLong(l);
	mrf.writeUnsignedLong(ul);
	mrf.writeFloat(f);
	mrf.writeString(str);
	mrf.writeBytes(buf, 3);

	// Send packet to the module with address 2
	mrf.sendPacket(4, true);

  while (!mrf.transmissionDone()){}
  if(mrf.transmissionSuccess()) {
    Serial.print("Success: ");
    Serial.println(b);
  }
  else {
    Serial.print("failed: ");
    Serial.println(b);
  }
	while (!mrf.transmissionSuccess()) {
		Serial.print("Trying: "); Serial.println(retry);
		retry++;
	}

	if (retry >= 5) Serial.println("Retry failed."); else Serial.println("Retry success");
	retry = 0;
	delay(100);
}
